#!/usr/bin/python
import json
import optparse
import os
import random
import sys
import threading
import time

import requests
from bs4 import BeautifulSoup


def say(sentence, speed, limit):
    os.system('spd-say -w -r {0} "{1}"'.format(speed, sentence))
    time.sleep(limit)
    return


def get_text(obj):
    if isinstance(obj, str) or isinstance(obj, unicode):
        return obj
    else:
        return obj.text


def get_link(obj):
    if obj.find('a'):
        return obj.find('a')
    else:
        return obj.text


def choose_statement():
    #  0 - place, 1 - team name, 2 - points
    statements = json_data['dialogues']
    return statements[random.randint(0, 1000000000000000000) % len(statements)]


def cancel_speech():
    os.system('spd-say -C')
    time.sleep(0.5)


def say_team_data(pos, speech_data):
    stmt = choose_statement()
    say(stmt.format(nums[pos], speech_data['name'].encode('utf-8'), speech_data['pts']),
        -40, 1)


class SpeechThread(threading.Thread):
    def __init__(self, speeches):
        threading.Thread.__init__(self)
        self.speeches = speeches

    def run(self):
        for speech in self.speeches:
            say(speech['sentence'], speech['speed'], 0.5)
        return

if __name__ == '__main__':
    f = open('data.json')
    json_data = json.loads(f.read())
    nums = json_data['nums']
    parser = optparse.OptionParser()
    for switch in json_data['switches']:
        parser.add_option(switch['type'], help=switch['help'], dest=switch['dest'], action=switch['action'])

    opts, args = parser.parse_args()

    # If league is unspecified, application is terminated
    if opts.league_code is None:
        say('Sorry', -20, 1)
        say('Specify a country code to find the league table standings', -30, 0)
        sys.exit(0)

    my_league = None

    for league in json_data['leagues']:
        if league['league_code'] == opts.league_code:
            my_league = league

    # League code is unrecognised
    if my_league is None:
        say('Sorry', -20, 1)
        say('The given league does not exist', -30, 0)
        sys.exit(0)

    start, end = 1, my_league['league_size']  # By default, feed is set to return details of all teams

    if opts.start is not None:
        opts.start = int(opts.start)
        if start <= opts.start <= end:
            start = opts.start

    if opts.end is not None:
        opts.end = int(opts.end)
        if start <= opts.end <= end:
            end = opts.end

    pid = os.fork()

    if pid == 0:
        say('Please wait while we load the data', -30, 0.5)
    else:
        try:
            os.waitpid(pid, 0)
            response = None
            if not opts.no_intro:
                sentences = json_data['credits']
                talk_thread = SpeechThread(sentences)
                talk_thread.start()
                response = requests.get(my_league['table_url'])
                talk_thread.join()
            else:
                response = requests.get(my_league["table_url"])

            if response.status_code != 200:
                say('Sorry', -20, 1)
                say('There was a problem', -20, 1)
                say('Try again later', -20, 1)
            else:
                data = []
                soup = BeautifulSoup(response.content, 'lxml')
                name_list = map(get_text, map(get_link, soup.findAll('td', {'class': 'team'})))
                gd_list = map(int, map(get_text, soup.findAll('td', {'class': 'gd'})))
                pts_list = map(int, map(get_text, soup.findAll('td', {'class': 'pts'})))

                gp_list = []
                won_list = []
                draw_list = []
                lost_list = []

                entries = soup.findAll('td', {'class': 'groupA'})

                for idx, val in enumerate(entries):
                    if idx % 6 == 0:
                        gp_list.append(int(val.text))
                    elif idx % 6 == 1:
                        won_list.append(int(val.text))
                    elif idx % 6 == 2:
                        draw_list.append(int(val.text))
                    elif idx % 6 == 3:
                        lost_list.append(int(val.text))

                for i in range(start-1, end):
                    data.append(dict(name=name_list[i], gp=gp_list[i], won=won_list[i], drew=draw_list[i],
                                     lost=lost_list[i], gd=gd_list[i], pts=pts_list[i]))

                say('Let us take a look at the current standings in {0}'.format(my_league['league_name']), -30, 0.5
                    )

                for idx, team_data in enumerate(data):
                    say_team_data(idx+start-1, team_data)

                say('Thank you for listening!', -40, 0)

        except requests.RequestException:
            pid1 = os.fork()
            if pid1 == 0:
                cancel_speech()
            else:
                os.waitpid(pid1, 0)
                say('Sorry', -40, 0.5)
                say('Internet connection failed', -40, 0)
            sys.exit(0)